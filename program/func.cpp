/**
 *
 *
 *  TODO
 *
 *
 *
 */
#include <iostream>
#include <list>
#include <vector>
#include <fstream>
#include <string>
#include "task.hpp"
#include "func.hpp"
#include "LesData3.h"
using namespace std;

extern int gTotalTasks;
extern vector <Task*> gTasks;



/**
 *
 *  newTask() - adds a new task to the vector, using the tasks constructor
 *
 */
void newTask()
{
    Task* newTask = nullptr;
    newTask = new Task;
    gTasks.push_back(newTask);
}

/**
 *  dateConvert - converts the date number to a more readable version
 *
 *  @param n - the number before conversion
 */
void dateConvert(const int n)
{
    //Switch seems more like a brute force method, but it is what i used (t)
    switch (n/100) {
        case 1: cout << "January ";         break;
        case 2: cout << "February ";        break;
        case 3: cout << "March ";           break;
        case 4: cout << "April ";           break;
        case 5: cout << "May ";             break;
        case 6: cout << "June ";            break;
        case 7: cout << "July ";            break;
        case 8: cout << "August ";          break;
        case 9: cout << "September ";       break;
        case 10: cout << "October ";        break;
        case 11: cout << "November ";       break;
        case 12: cout << "December ";       break;
    }

    //gets the day value from the int provided
    int dayValue = n%100;
    cout << dayValue;
    switch(dayValue%10)
    {
        case 1: cout << "st";       break;
        case 2: cout << "nd";       break;
        case 3: cout << "rd";       break;
        default: cout << "th";      break;
    }

}

/**
 *  finishTask - sets a task to be finished
 */
void finishTask() {

    //makes sure there is a task in the system
    if (gTasks.size() <= 0) {

        cout << "\n\tNo tasks registered.\n\n";

    } else {

        //gets the user to choose one of the tasks in the system
        int choice = lesInt("Which task would you like to finish?",
                                                            1, gTasks.size());
        cout << endl;

        if(gTasks[choice-1]->isFinished())
            cout << "\tTask already set to finished.\n\n";
        else
        {
            //sets that task to be finished
            gTasks[choice-1]->setFinished();
                    //prints confirmation message that it is completed
            cout << "\n\tTask nr. " << choice << " finished. Good job!\n\n";
        }
    }
}

/**
 *  changeData - opens a menu that allows the user to select a task to change data about
 */

void changeData()
{
    if(gTasks.size() == 0)
        cout << "\n\tNo tasks registered.\n" << endl;
    else
    {

        // LIST TASKS HERE
        //Gets user to input what task to be manipulated
        int taskNum = lesInt("\nWhich task would you like to change?", 1, gTasks.size());
        cout << "\n\tTask number " << taskNum << " selected.\n" << endl;

        //Menu for data that can be manipulated
        cout << "What would you like to change about the task?\n" <<
        "\tP - Change priority\n" <<
        "\tD - Change deadline\n" <<
        "\tR - Remove/Delete task\n";

        char changeFeature = lesChar("Command");
        cout << endl;

        //switch statement to determine what menu choice was taken
        switch (changeFeature)
        {
            case 'P': changePriority(taskNum-1); break;
            case 'D': changeDeadline(taskNum-1); break;
            case 'R': deleteTask(taskNum-1);     break;
            default: cout << "Invalid command." << endl;
        }
    }
}

/**
*   Deletes/removes a task completely from the list
*
*   @param tasknum - The number of the task which is going to be deleted
*/
void deleteTask(int tasknum)
{
    cout << "\n\tRemoving a task will delete it permanently from the registered tasks."
         << "\n\tThis is not possible to undo!\n";

    cout << "\n\tAre you sure you would like to remove task number " << tasknum+1 << "?(Y/N)";
    char ans = lesChar("");

    if (ans == 'Y')
    {
        gTasks[tasknum]->deleteTask();
        gTasks[tasknum] = gTasks.back();
        gTasks.pop_back();
        cout << "\n\tTask number " << tasknum+1 << " deleted.\n\n";
    }
    else
    {
        cout << "\n\tNo task deleted. To see all the registered tasks, use command L.\n\n";
    }
}

/**
 *void readDate(setTime) - reads in a month and day from user, getting day limits from months
 *
 *  When using this data, you will need to divide by 100 to get month, and do modulo 100 to get the day
 *
 *@var - setTime - The time we wish to set
 */
int readDate() {
    int month, day, res;
    month = lesInt("\n\tMonth", 1, 12);

    //My attempt at being clever with the if statements was not correct
    //and my fix did not work. so this is a brute force way of doing it
    switch (month) {

        //Default is any month with 31 days
        case 2 :    day = lesInt("\tday  ", 1, 28); break;
        case 4 : case 6: case 9: case 11:
                    day = lesInt("\tday  ", 1, 30); break;
        default:    day = lesInt("\tday  ", 1, 31); break;

    }
//    if (month % 2 == 1) {
//        day = lesInt("day  ", 1, 31);
//    } else if (month == 2) {
//        day = lesInt("day  ", 1, 28);
//    } else {
//        day = lesInt("day  ", 1, 30);
//    }
    //Sets formatting to be in line with formatting defined in header
    return res = month*100+day;
}

/**
 *  Reading tasks from file, and places them in 'gTasks'
 *
 *  @see  Tasks::Task(...)
 */
void readFromFile() {

    ifstream infile("tasks.dta");       // Opens file
    Task* newTask = nullptr;           // Sets new pointer to nullptr

    string title;                       // Title of task

    if(infile) {                        // The file is found
        cout << "Reading from file 'TASKS.DTA' .......\n\n";
        infile >> gTotalTasks; infile.ignore();          // Reads total tasks
     //   getline(infile, title);        // Reads title

        for(int i = 0; i < gTotalTasks; i++)
        {
            newTask = new Task(infile); // Makes the new task
            gTasks.push_back(newTask);  // Pushes the new task into the list
        }
        infile.close();                 // Closes the file
    }
    else                                // The file is not found
        cout << "\n\nDid not find the file 'TASKS.DTA'!\n\n";
}

/**
 *  writeMenu() - prints the main menu to the screen
 */
void writeMenu()
{
    cout << "Available options:\n"      <<
    "\t(L)ist all tasks\n"              <<
    "\t(N)ew Task\n"                    <<
    "\t(E)dit/Remove task\n"            <<
    "\t(F)inish task\n"                 <<
    "\t(R)eorder based on priority\n"   <<
    "\t(Q)uit and save\n";
}

/**
 *  Writes the whole datastructure to file.
 *
 *  @see  Tasks::writeToFile(...)
 */
void writeToFile()
{
    ofstream outfile("tasks.dta");      // Opens file

    cout << "\nWriting to file 'TASKS.dta' .......\n\n";

    outfile << gTasks.size() << "\n";

    for (int i = 0; i < gTasks.size(); i++)
    {
        gTasks[i]->writeToFile(outfile);   // Each task writes to file
    }

    outfile.close();                    // Closes the file

}

/**
 *  reorderOnPrio - reoderd the vector based on priority, higher prio goes first
 */
void reorderOnPrio()
{
    if (gTasks.size() < 2)
    {

        cout << "\n\tNot enough tasks to reorder.\n\n";

    } else
    {
        vector <Task*> tempVector;

        //makes an iterator that will go through the current tasks in the list
        auto taskIt = gTasks.begin();

        //goes through all the tasks and checks if the prio is 5
        for ( ; taskIt != gTasks.end(); taskIt++)
        {
            if ((*taskIt)->getPrio() == 5)
            {
                //if the prio is 5 it adds it to the back of the temp vector
                tempVector.push_back((Task*) *taskIt);
            }
        }

        //sets iterator back to the beginning
        taskIt = gTasks.begin();

        //goes and looks for tasks with prio 4
        for ( ; taskIt != gTasks.end(); taskIt++) {

            if ((*taskIt)->getPrio() == 4) {

                //adds them to the end
                tempVector.push_back((Task*) *taskIt);

            }
        }

        //resets iterator
        taskIt = gTasks.begin();
        for ( ; taskIt != gTasks.end(); taskIt++) {

            //looks for task with prio 3
            if ((*taskIt)->getPrio() == 3) {

                //adds to the back
                tempVector.push_back((Task*) *taskIt);

            }
        }

        //resets iterator
        taskIt = gTasks.begin();
        for ( ; taskIt != gTasks.end(); taskIt++) {

            //checks for tasks with prio 2
            if ((*taskIt)->getPrio() == 2) {

                //adds to the end of vector
                tempVector.push_back((Task*) *taskIt);

            }
        }

        //resets iterator
        taskIt = gTasks.begin();
        for ( ; taskIt != gTasks.end(); taskIt++) {

            //checks for tasks with prio 1
            if ((*taskIt)->getPrio() == 1) {

                //adds to the end of temp vector
                tempVector.push_back((Task*) *taskIt);

            }
        }

        //Changes the actual vector with the now sorted vector.
        gTasks = tempVector;

        //confirmation message
        cout << "\n\tTasks reordered based on priority!\n\n";
    }

}

/**
 *  changePriority - Allows the user to change the priority of  a task
 *
 *  @param taskNum - the place of the task in the global vector to be manipulated
 */
void changePriority(int taskNum)
{
    cout << "Enter new priority ";
    gTasks[taskNum]->readPriority();
    cout << "\n\tPriority changed.\n\n";
}

/**
 *  changeDeadline - Allows the user to change the deadline of a task in the system
 *
 *  @param taskNum - the place of the task in the global vector to be manipulated
 */
void changeDeadline (int taskNum)
{
    cout << "Enter new deadline: ";
    gTasks[taskNum]->readDeadline();
    cout << "\n\tDeadline changed.\n\n";
}


/**
 *  listTasks - lists all tasks currently in the vector
 */
void listTasks()
{
    //Makes sure there is a task in the system
    if(gTasks.size() == 0)
        cout << "\n\tNo tasks registered.\n\n";
    else
    {
        cout << "\nCurrent registered tasks: \n";
        for(int i = 0; i < gTasks.size(); i++)
        {
            //Prints out all tasks, one by one
            cout << "Task " << i+1 << ":\n";
            gTasks[i]->writeData();
            cout << "\n";
        }
        cout << endl;
    }

}
//**************************************************************************
