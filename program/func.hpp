#ifndef func_hpp
#define func_hpp

void changeData();
void dateConvert(const int n);
void deleteTask(int);
void finishTask();
void newTask();
int  readDate();
void readFromFile();
void writeMenu();
void writeToFile();
void reorderOnPrio();
void changePriority(int);
void changeStatus(int);
void changeDeadline(int);
void listTasks();

#endif /* func_hpp */
