/**
 *  This is a TODO list program that displays a list of current actions to do
 *
 *
 *
 *
 *  @Author Thomas Øvsttun Ditman, Anna Pham, Martin Hyldmo,
 *          Omar Mohammad Farah, and Stian Tusvik
 *  @file main.cpp
 *  @date 26.03.2021
 */

#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include "func.hpp"
#include "task.hpp"
#include "LesData3.h"
using namespace std;

///< Global variables declared
int gTotalTasks;
vector <Task*> gTasks;


int main ()
{
    char command;

    // Reads the already made tasks from file
    readFromFile();

    //Function to print TODO list
    cout << "\nNO-CRASTINATION\n\n";
    writeMenu();
    command = lesChar("Command");

    //Main menu switch
    while (command != 'Q')
    {
        switch (command)
        {
            //Fill in as functions are created
            case 'L': listTasks();             break;
            case 'N': newTask();               break;
            case 'E': changeData();            break;
            case 'F': finishTask();            break;
            case 'R': reorderOnPrio();         break;
            case 'Q':                          break;
            default: cout << "\n\tInvalid command.\n" << endl;
        }
        writeMenu();
        command = lesChar("Command");
    }

    writeToFile();
    cout << "\n\tAll tasks saved.\n"  <<
            "\tQuitting program.\n\n\n";

    return 0;
}
