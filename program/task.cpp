/**
 *  These are the functions within the Task class
 *
 */
#include <iostream>
#include "enum.hpp"
#include "func.hpp"
#include "task.hpp"
#include "LesData3.h"
using namespace std;

int cat;

/**
 *  Task::Task - Blank task constructor, not meant to be used in normal situations
 */
Task :: Task()
{
    Task::readData();
    cout << "\n\tNew task added.\n\n";
}

/**
 *  Task::Task(ifstream & in) - Constructor used in conjunction with reading from file
 *
 *  @param in - The file we are reading from
 */
Task :: Task(ifstream & in)
{
    int cat;
    getline(in, name);
    getline(in, description);
    in >> priority >> cat; in.ignore();
    in >> startTime >> deadline; in.ignore();
    in >> finishTime; in.ignore();

    if(finishTime == 0)
        status = false;
    else
        status = true;

    switch(cat)
    {
        case 1: category = houseWork;       break;
        case 2: category = schoolwork;      break;
        case 3: category = work;            break;
        case 4: category = shopping;        break;
        case 5: category = other;           break;
        default: cout << "Category reading error." << endl;
    }
}

/**
 *  Task::writeToFile - function writes it's data to the file of choice
 *
 *  @param out - The file we are printing to
 */
void Task::writeToFile(ofstream & out)
{
    int cat;
    switch(category)
    {
        case houseWork:         cat = 1; break;
        case schoolwork:        cat = 2; break;
        case work:              cat = 3; break;
        case shopping:          cat = 4; break;
        case other:             cat = 5; break;
    }

    out << name << "\n";
    out << description << "\n";
    out << priority << " " << cat << "\n";
    out << startTime << " " << deadline << "\n";

    if(finishTime == 0)
        out << 0;
    else
        out << finishTime;

    out << "\n";
}

/**
*   Task::readData - function reads all relevant data into the task
*
*/

/**
 *  getPrio - returns priority value
 */
int Task::getPrio() {

    return priority;

}

/**
 * readData - Gets the user to input all needed data into a task
 */
void Task::readData()
{
    //enters name into task
    cout << "\nEnter the name of the task: ";
    getline(cin, name);

    //enters description into task
    cout << "\nEnter the description of the task: ";
    Task::readDescription();

    //enters startdate for timeframe into task
    cout << "\nEnter the start date of the task: ";
    startTime = readDate();

    //enters deadline for task into task
    cout << "\nEnter the deadline for the task: ";
    Task::readDeadline();

    //enters in priority of task on a scale from 1 to 5
    cout << "\nEnter the priority of the task ";
    Task::readPriority();

    cout << "\nChoose the category of the task.\n\n";
    Task::readCategory();

    finishTime = 0;
    status = false;
}

/**
 * setFinished - sets a task to be finished, and gets the user to input a date for when it was finished
 */
void Task::setFinished()
{
    status = true;

    //runs a function to put in finish time
    readFinishTime();

}


/**
 *  readDescription - Gets user to input description of a task
 */
void Task::readDescription()
{
    getline(cin, description);
}

/**
 *  readFinishTime - Gets user to input the finish time for the completed task
 */
void Task::readFinishTime()
{
    cout << "Enter the date the task got finished";
    finishTime = readDate();

    status = true;
}


/**
 *  readDeadLine - Gets user to input deadline for the task
 */
void Task::readDeadline()
{
    deadline = readDate();
}

/**
 *  readCategory - Gets the user to input the category for the task
 */
void Task::readCategory()
{
    //print available choices
    cout << "\tC - Housework\n\tS - Schoolwork" <<
            "\n\tW - Work\n\tH - Shopping\n\tO - Other\n\n";
    char choice;

    //gets a choice in the form of a char
    choice = lesChar("Choice");
    switch (choice)
    {
        //converts char to a category
        case 'C': category = houseWork;     break;
        case 'S': category = schoolwork;    break;
        case 'W': category = work;          break;
        case 'H': category = shopping;      break;
        case 'O': category = other;         break;
        default: choice = lesChar("\nChoice");
    }
}

/**
 *  readPriority - Gets the user to input a priority for the task
 */
void Task::readPriority()
{
    priority = lesInt("(1 = lowest, 5 = highest)", 1, 5);
}

/**
 *  writeData - Prints out the data of the current working task.
 */
void Task::writeData()
{
    cout << "\tName: " << name << endl;
    cout << "\tDescription: " << description << endl;
    writeCategory();
    cout << "\tPriority: " << priority << endl;
    cout << "\tStart date: "; dateConvert(startTime); cout << endl;
    cout << "\tDeadline: "; dateConvert(deadline); cout << endl;

    string currentStatus = (status) ? "Finished" : "Unfinished";
    cout << "\tStatus: " <<  currentStatus << endl;

    if(status) {
        cout << "\tDate completed: "; dateConvert(finishTime); cout << endl;
    }

}

void Task::writeCategory()
{
    cout << "\tCategory: ";
    switch(category)
    {
        case houseWork:     cout << "Housework"     << endl; break;
        case schoolwork:    cout << "Schoolwork"    << endl; break;
        case shopping:      cout << "Shopping"      << endl; break;
        case work:          cout << "Work"          << endl; break;
        case other:         cout << "Other"         << endl; break;
    }
}

bool Task::isFinished()
{
    return status;
}

void Task::deleteTask()
{
    delete this;
}
