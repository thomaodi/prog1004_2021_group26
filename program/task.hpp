/**
 *
 *
 *
 *  TODO
 *
 *
 *
 */
#ifndef list_hpp
#define list_hpp
#include <string>
#include <fstream>
#include "enum.hpp"

class Task
{
private:
    std::string name,
        description;
    //Start and finish time are in the format (monthday) ex: 1231 for 31. dec
    int startTime,
        finishTime,
        deadline,
        priority;
    bool status;
    enum Category category;

public:
    //Class functions
    Task();
    Task(std::ifstream & in);
    void writeToFile(std::ofstream & out);
    int  getPrio();
    void readData();
    void readCategory();
    void readDeadline();
    void readDescription();
    void readFinishTime();
    void readPriority();
    void setFinished();
    void writeData();
    void changeStatus();
    void changeStatus(int);
    void writeCategory();
    bool isFinished();
    void deleteTask();
};



#endif /* list_hpp */
